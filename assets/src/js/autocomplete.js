const guestData = [];
const nameInput = document.querySelector('#field_qh4icy2');
const idInput = document.querySelector('#field_yq3xp');
const languageInput = document.querySelector( '#field_nrs2d');
const suggestionsContainer = document.createElement( 'ul' );
const germanQuestions = [117,118,123,125,120,121,127,104];
const dutchQuestions = [73,82,93,94,84,92,101,128];
suggestionsContainer.id = 'field_qh4icy2_suggestions';
nameInput.parentNode.appendChild( suggestionsContainer );

function findMatches( wordToMatch, guestData ) {
    return guestData.filter( guest => {
        const regex = new RegExp( wordToMatch, 'gi' );
        return guest.title.rendered.match( regex );
    } )
}

function handleKeyDown( e ) {
    if ( e.which === 13 ) {
        e.preventDefault();
    }
}

function handleKeyInput( e ) {
    
    if ( this.value.length < 2 ) {
        suggestionsContainer.innerHTML = '';
        return;
    }
    
    const key = e.which,
    value = this.value;

    //if ( key == 40 || key == 38 || key == 13 ) {
    if ( key == 40 || key == 13 ) {
        navigateThroughMatches( key );
    } else {
        displayMatches( value );
    }
}

function acceptSelection( props ) {
    nameInput.value = props.value;
    idInput.value = props.id;
    languageInput.value = props.lang;
    languageInput.dataset.frmval = props.lang;
    suggestionsContainer.innerHTML = '';

    // if ( props.lang === 'NL' ) {
    //     toggleDutchQuestions();
    // } 
}

function displayMatches( value ) {
    
    let html = '';

    if ( ! value.length < 2 ) {
        
        const matchArray = findMatches( value, guestData );

        if ( matchArray.length > 1 || ! suggestionsContainer.hasChildNodes ) {
            suggestionsContainer.innerHTML = html;
            return;
        }

        html = matchArray.map( guest => {
            return `<li class="guest" data-language="${guest.language.name}" data-post_id="${guest.id}">${guest.title.rendered}</li>`;
        }).join('');
        
        suggestionsContainer.innerHTML = html;

    }
    
}

function navigateThroughMatches( key ) {
    
    if ( ! suggestionsContainer.hasChildNodes ) {
        return;
    }

    const matchedGuests = Array.from(suggestionsContainer.childNodes);
    
    let selectedSuggestion;

    if ( matchedGuests.length === 1 ) {
        selectedSuggestion = matchedGuests[0];
        selectedSuggestion.classList.add( "selected" );
    } else {
        selectedSuggestion = matchedGuests.filter( elem => {
            return elem.classList.length && elem.classList.contains( "selected" );
        })
    }

    // Catching the enter;
    if ( key == 13 || key == 40 ) {
        console.log( selectedSuggestion );
        acceptSelection( {
            value: selectedSuggestion.textContent,
            id   : selectedSuggestion.dataset.post_id,
            lang : selectedSuggestion.dataset.language
        });
        return;
    }
    
    // if ( selectedSuggestion.length ) {
    //     selected[0].classList.remove( "selected");
    // }

    // let newlySelected;
    
    // if ( key == 40 ) {
    //     // Down key
    //     if ( ! selected.length || ! selected[0].nextElementSibling ) {
    //         matchedGuests[0].classList.add( "selected" );
    //     } else {
    //         newlySelected = selected[0].nextElementSibling;
    //         newlySelected.classList.add( "selected" );
    //     }
    // } else if ( key == 38 ) {
    //     // Up key
    //     if ( ! selected.length || ! selected[0].previousElementSibling ) {
    //         matchedGuests[0].classList.add( "selected" );
    //     } else {
    //         newlySelected = selected[0].previousElementSibling;
    //         newlySelected.classList.add( "selected" );
    //     }
    // }
}

nameInput.addEventListener( 'change', handleKeyInput );
nameInput.addEventListener( 'keyup', handleKeyInput );
nameInput.addEventListener( 'keydown', handleKeyDown );

(function($) {

    $(document).ready( function() {


        // $.ajax( {
        //     url: wpApiSettings.root + 'wp/v2/guest?per_page=60',
        //     method: 'GET',
        //     beforeSend: function ( xhr ) {
        //         xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
        //     }
        // } )
        // .error( function( e ) {
        //     console.error( e );
        // } )
        // .done( function ( response ) {
        //     console.log( response );
        // } );
        if ( $( '.frm_form_fields' ).hasClass( 'frm_page_num_1' ) ) {
            $('#post-70 > audio').prop('muted',false);
        }

        fetch( wpApiSettings.root + 'wp/v2/guest?per_page=60' )
        .then( response => response.json() )
        .then( data => guestData.push( ...data ) );
        
    });

    $(suggestionsContainer).on('click', 'li', function() {
        acceptSelection( {
            value: $(this).text(),
            id   : $(this).data('id'),
            lang : $(this).data('language')
        } );
    })

    $('.frm_button_submit').one('click', function( e ) {
        e.preventDefault();
        $('#post-70 > audio').prop('muted',true);
        $(this).trigger('click');
    })

})(jQuery);