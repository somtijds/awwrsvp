<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Awwrsvp
 */

?>

	</div><!-- #content -->

	<?php if ( ! is_page( array( 'rsvp', 'home', 'moments', 'friday', 'saturday' ) ) ) : ?>

		<footer id="colophon" class="site-footer">
			<div class="site-info">
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'awwrsvp' ) ); ?>">
					<?php
					/* translators: %s: CMS name, i.e. WordPress. */
					printf( esc_html__( 'Proudly powered by %s', 'awwrsvp' ), 'WordPress' );
					?>
				</a>
				<span class="sep"> | </span>
					<?php
					/* translators: 1: Theme name, 2: Theme author. */
					printf( esc_html__( 'Theme: %1$s by %2$s.', 'awwrsvp' ), 'awwrsvp', '<a href="http://underscores.me/">Willem Prins | Somtijds</a>' );
					?>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->

	<?php endif; ?>

</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>
