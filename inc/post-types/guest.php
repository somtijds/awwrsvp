<?php

/**
 * Registers the `guest` post type.
 */
function awwrsvp_guest_init() {
	register_post_type( 'guest', array(
		'labels'                => array(
			'name'                  => __( 'Guests', 'awwrsvp' ),
			'singular_name'         => __( 'Guest', 'awwrsvp' ),
			'all_items'             => __( 'All Guests', 'awwrsvp' ),
			'archives'              => __( 'Guest Archives', 'awwrsvp' ),
			'attributes'            => __( 'Guest Attributes', 'awwrsvp' ),
			'insert_into_item'      => __( 'Insert into Guest', 'awwrsvp' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Guest', 'awwrsvp' ),
			'featured_image'        => _x( 'Featured Image', 'guest', 'awwrsvp' ),
			'set_featured_image'    => _x( 'Set featured image', 'guest', 'awwrsvp' ),
			'remove_featured_image' => _x( 'Remove featured image', 'guest', 'awwrsvp' ),
			'use_featured_image'    => _x( 'Use as featured image', 'guest', 'awwrsvp' ),
			'filter_items_list'     => __( 'Filter Guests list', 'awwrsvp' ),
			'items_list_navigation' => __( 'Guests list navigation', 'awwrsvp' ),
			'items_list'            => __( 'Guests list', 'awwrsvp' ),
			'new_item'              => __( 'New Guest', 'awwrsvp' ),
			'add_new'               => __( 'Add New', 'awwrsvp' ),
			'add_new_item'          => __( 'Add New Guest', 'awwrsvp' ),
			'edit_item'             => __( 'Edit Guest', 'awwrsvp' ),
			'view_item'             => __( 'View Guest', 'awwrsvp' ),
			'view_items'            => __( 'View Guests', 'awwrsvp' ),
			'search_items'          => __( 'Search Guests', 'awwrsvp' ),
			'not_found'             => __( 'No Guests found', 'awwrsvp' ),
			'not_found_in_trash'    => __( 'No Guests found in trash', 'awwrsvp' ),
			'parent_item_colon'     => __( 'Parent Guest:', 'awwrsvp' ),
			'menu_name'             => __( 'Guests', 'awwrsvp' ),
		),
		'public'                => false,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => false,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'guest',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'awwrsvp_guest_init' );

/**
 * Sets the post updated messages for the `guest` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `guest` post type.
 */
function awwrsvp_guest_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['guest'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Guest updated. <a target="_blank" href="%s">View Guest</a>', 'awwrsvp' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'awwrsvp' ),
		3  => __( 'Custom field deleted.', 'awwrsvp' ),
		4  => __( 'Guest updated.', 'awwrsvp' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Guest restored to revision from %s', 'awwrsvp' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Guest published. <a href="%s">View Guest</a>', 'awwrsvp' ), esc_url( $permalink ) ),
		7  => __( 'Guest saved.', 'awwrsvp' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Guest submitted. <a target="_blank" href="%s">Preview Guest</a>', 'awwrsvp' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Guest scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Guest</a>', 'awwrsvp' ),
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Guest draft updated. <a target="_blank" href="%s">Preview Guest</a>', 'awwrsvp' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'awwrsvp_guest_updated_messages' );
