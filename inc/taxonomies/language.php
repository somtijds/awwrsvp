<?php

/**
 * Registers the `language` taxonomy,
 * for use with 'guest'.
 */
function language_init() {
	register_taxonomy( 'language', array( 'guest' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Languages', 'awwrsvp' ),
			'singular_name'              => _x( 'Language', 'taxonomy general name', 'awwrsvp' ),
			'search_items'               => __( 'Search Languages', 'awwrsvp' ),
			'popular_items'              => __( 'Popular Languages', 'awwrsvp' ),
			'all_items'                  => __( 'All Languages', 'awwrsvp' ),
			'parent_item'                => __( 'Parent Language', 'awwrsvp' ),
			'parent_item_colon'          => __( 'Parent Language:', 'awwrsvp' ),
			'edit_item'                  => __( 'Edit Language', 'awwrsvp' ),
			'update_item'                => __( 'Update Language', 'awwrsvp' ),
			'view_item'                  => __( 'View Language', 'awwrsvp' ),
			'add_new_item'               => __( 'New Language', 'awwrsvp' ),
			'new_item_name'              => __( 'New Language', 'awwrsvp' ),
			'separate_items_with_commas' => __( 'Separate Languages with commas', 'awwrsvp' ),
			'add_or_remove_items'        => __( 'Add or remove Languages', 'awwrsvp' ),
			'choose_from_most_used'      => __( 'Choose from the most used Languages', 'awwrsvp' ),
			'not_found'                  => __( 'No Languages found.', 'awwrsvp' ),
			'no_terms'                   => __( 'No Languages', 'awwrsvp' ),
			'menu_name'                  => __( 'Languages', 'awwrsvp' ),
			'items_list_navigation'      => __( 'Languages list navigation', 'awwrsvp' ),
			'items_list'                 => __( 'Languages list', 'awwrsvp' ),
			'most_used'                  => _x( 'Most Used', 'language', 'awwrsvp' ),
			'back_to_items'              => __( '&larr; Back to Languages', 'awwrsvp' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'language',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'language_init' );

/**
 * Sets the post updated messages for the `language` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `language` taxonomy.
 */
function language_updated_messages( $messages ) {

	$messages['language'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Language added.', 'awwrsvp' ),
		2 => __( 'Language deleted.', 'awwrsvp' ),
		3 => __( 'Language updated.', 'awwrsvp' ),
		4 => __( 'Language not added.', 'awwrsvp' ),
		5 => __( 'Language not updated.', 'awwrsvp' ),
		6 => __( 'Languages deleted.', 'awwrsvp' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'language_updated_messages' );


function awwrsvp_add_language_to_guest_response() {
	register_rest_field (
		'guest', 
		'language', //New Field Name in JSON RESPONSEs
		array(
    		'get_callback'    => 'awwrsvp_get_language_for_api', // custom function name 
    		'update_callback' => null,
    		'schema'          => null,
     	)
	);
}
add_action( 'rest_api_init', 'awwrsvp_add_language_to_guest_response' );

function awwrsvp_get_language_for_api( $object ) {
    //get the id of the post object array
    $post_id = $object['id'];
 
	//return the language taxonomy value
	$language = get_the_terms( $post_id, 'language' );

	if ( is_array( $language ) ) {
		return $language[0];
	}
}