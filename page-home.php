<?php
/**
 * The HOME template
 *
 * Template name: HOME
 *
 * @package Awwrsvp
 */

?>

	<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115190711-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
    
            gtag('config', 'UA-115190711-1');
        </script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>WEDDING WEDDING DOT LOVE: Annegret 💘  Willem</title>
        <meta property="og:title" content="WEDDING WEDDING DOT LOVE" />
        <meta property="og:description" content="Hier bouwt Somtijds aan de website voor WEDDING WEDDING DOT LOVE" />
        <meta property="og:url" content="https://www.weddingwedding.love" />
        <meta property="og:image" content="" />
        <meta name="description" content="WEDDING WEDDING DOT LOVE: Annegret 💘 Willem">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Titan+One" rel="stylesheet">
        <link href='' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() . '/assets/css/awww.css' ); ?>">
	</head>
	<body class="noscroll">
   	 	<!--[if lt IE 9]>
        	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="container">
            <div class="header-wrapper">
                <header>
                    <a href="<?php echo esc_url( get_permalink( get_page_by_title('rsvp') ) ); ?>" title="RSVP">
                        <video autoplay playsinline muted loop preload poster="<?php echo esc_url( get_template_directory_uri() . '/assets/img/dogs-from-animals-on-Instagram.jpeg' ); ?>">
                            <source src="<?php echo esc_url( get_template_directory_uri() . '/assets/videos/dogs-from-animals-on-Instagram.mp4' ); ?>" />
                        </video>
                    </a>
                    <svg width="100%" height="100%" viewBox="0 0 700 700" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg">
                        <defs>
                            <clipPath clipPathUnits="objectBoundingBox" id="clipPath" transform="scale(0.00155,0.00172)">
                                <path d="M 297.29747,550.86823 C 283.52243,535.43191 249.1268,505.33855 220.86277,483.99412 C 137.11867,420.75228 125.72108,411.5999 91.719238,380.29088 C 29.03471,322.57071 2.413622,264.58086 2.5048478,185.95124 C 2.5493594,147.56739 5.1656152,132.77929 15.914734,110.15398 C 34.151433,71.768267 61.014996,43.244667 95.360052,25.799457 C 119.68545,13.443675 131.6827,7.9542046 172.30448,7.7296236 C 214.79777,7.4947896 223.74311,12.449347 248.73919,26.181459 C 279.1637,42.895777 310.47909,78.617167 316.95242,103.99205 L 320.95052,119.66445 L 330.81015,98.079942 C 386.52632,-23.892986 564.40851,-22.06811 626.31244,101.11153 C 645.95011,140.18758 648.10608,223.6247 630.69256,270.6244 C 607.97729,331.93377 565.31255,378.67493 466.68622,450.30098 C 402.0054,497.27462 328.80148,568.34684 323.70555,578.32901 C 317.79007,589.91654 323.42339,580.14491 297.29747,550.86823 z" />
                            </clipPath>
                        </defs>
                    </svg>
                </header>
            </div>
        </div>

<?php
get_footer();
