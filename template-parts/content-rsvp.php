<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Awwrsvp
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<audio autoplay loop muted>
		<source src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/audio/never-too-much.mp3' ); ?>" type="audio/mp3">
		<p>Your browser doesn't support HTML5 audio. Here is a <a href="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/audio/never-too-much.mp3' ); ?>">link to the audio</a> instead.</p>
	</audio>
	<div class="entry-content">
		<?php
		the_content();
		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
